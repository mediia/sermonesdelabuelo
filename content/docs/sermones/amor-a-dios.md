# Amor a Dios

## Introduccion 

1. El Monte Sinaí es uno de los más célebres en la historia de la humanida. No por lo que es en sí sino porque Dios lo úso como un medio para dar su ley, el Decálogo.

2. La ley moral o Decálogo es el más importante organismo que Dios dio a los seres humanos de todos los tiempos.

    1. Muestra el camino recto que el hombre debe seguir para ser salvo por la gracia de Cristo. El que ama a Dios automáticamente buscará hacer la voluntad de su Maestro, guardarás pues sus mandamientos.

    2. Es la norma de juicio de Dios.

    3. Resume el amor: Los cuatros primeros indican el amor que debemos a Dios y los seis últimos al hombre.

    4. Cada uno de los mandamientos de la Ley Moral o Decálogo se desglosan de tal forma que abarcan todas las áreas de la conducta humana.

    5. El infrigir uno de ellos significa como si lo hubíeramos hecho con todos ellos. Sucede como el que transgrede la ley del Estado: el infractor de una sola cláusula seré penado por la ley como si el Código estuviere en su contra.

    6. En la Ley de un Estado no se puede pnar "Sin juicio previo basado en ley anterior al hecho del proceso"(Art. 18 de nuetra constitución). Dios utilizará en el juicio final el decálogo.

## Los Tres Primeros Mandamientos del Decálogo

1. "No tendrás dioses ajenos"

    1. El uso del verbo en singular muestra que Dios se dirigía a cada individuo de la Nación. No es suficiente la obediencia colectiva.

    1. Todos los pueblos de la antiguedad eran politeístas.  
    Dios prohibío expresamente su adoración.

    1. Cualquier cosa que nos atraiga y que tienda a disminuir nuestro amor a Dios... es para nosotros un dios.

    1. Dioses de nuetra época: santos que suplantan la confianza que debe depositar en el verdadero Dios.

    1. Otros dioses populares: lo que ocupa el lugar de Dios: ciencia, dinero, modas, fama, honores, deportes, etc.  

2. "No te harás imagen"  

    1. Es otra prohibición expresa que pone énfasis en la naturaleza espiritual de Dios (Juan 4:23,24) y desprueba la idolatría y el materialismo.

    1. Isaías ridiculiza la fabricación de ídolos (Isa. 44:6 en adelante).  

    1. Este Mandamiento no prohibe el material religioso ilustrativo, sí condena la reverencia, adoración o semiadoración de los ídolos representados en pinturas o esculturas o cosa, tomados como religiosas.  

    1. "Niguna semejanza": Las expresiones en "el cielo, la tierra y el mar" indican universalidad y era donde estaban los dioses paganos y donde hoy se vinculan a algunas divinizaciones humanas. (Deut. 4:15-19 y Rom. 1:21-23).  
    
    1. "No te inclinarás": Condena la honra externa a las imágenes. Los adoradores consideran a la imagen como real conciente o inconscientemente o como morada de la divinidad idolatrada. Dios rehusa compartir su gloria con los Eidolos, es "celoso" (Isa. 42:8; 48:11).   
    
    1. "Visito la maldad": Dios no castiga al individuo por los malos hechos de otros (Eze. 18:2-24). Cada persona es respondable delante de Dios por sus actos (Ecl. 12:13,14). Todo lo que el hombre sembrare, segará (Gal. 6:7).  
    
    1. "Los que me aborrecen": ABORRECER es rehusar servirle, conociéndolo, es confiar en cualquier cosa que sea Dios. Expresa la más pfrofunda desaprobación de Dios. Aborrecerle es sencillo: Simplemnete es ama más a cosas o personas más que a El. (Luc. 14:26; Rom.9:13)  
    
    1. A los que son fieles se les promete el amor inmenso de Dios, no sólo hasata la cuarta generación sino hasta la milésima. (Deut. 7:9)  
    
    1. Este Mandamiento del Decálogo ha sido anulado por el "Catecismo". En su lugar se repitió el 10: "No codiciarás..." Estaba pronosticado. (Dan. 7:25).  

3. "No tomarás su nombre en vano"  

    1. Prohibe tomar su nombre en forma descuidada o frívola. Interjucciones, juramentos, etc.  
    
    1. El mismo incluira reverencias (Sal. 111:9; Ecl. 5:1,2) y actitud correcta con que se debe presentar o conducir en la casa de Dios: Vestiduras adecuadas, silencio reverente, el no empleo de cualquier "Masticable", fomenta el recogimiento y actitud de oración.  

## Conclusión

Si amamos a Dios no permitiremos que nada ni nadie se interponga entre El y nosotros.
