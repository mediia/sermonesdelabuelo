# 2/5 El Espíritu de Profecía

## EXORDIO


### Períodos intermitentes de los mensajes escritos de Dios

Dios ha usado el lenguaje escrito para sus mensajes.

#### Aun en los pueblos más antiguos se han descubierto sus escritos.

1. Por el lenguaje escrito DABAN DECRETOS LOS MONARCAS.

1. En la misma forma se dejaban los archivos legilativos y judiciales.

1. Este también ha sido el método de Dios para instruir a su pueblo.

> Tenemos el ANTIGUO Y NUEVO TESTAMENTO.

#### En los mensajes escrito de Dios, descubriemos PERIODOS DE SILENCIO.

1. Entre EL ANTIGUO Y NUEVO TESTAMENTO hubo casi 400 años de silencio.

1. Después de la actuación de LOS APOSTOLES que duró CIEN AÑOS, hemos tenido casi 2000 años de silencio.

1. Esto no quiere decir que Dios haya deseado dejaremos en ignorancia.

> La culpa debe ser del hombre

## CUERPO

### La verdad salvadora se conoce solo por el testimonio de Jesucristo

#### El testimonio de Jesucristo completa los dones.

> "Así como el testimonio de Cristo ha sido confirmado en vosotros: De tal manera que nada os falte en ningún don, esperando la manifestación de nuestro Señor Jesucristo" -- 1 Corintios 1:6-7

1. El testimonio de Nuestro Señor Jesucristo nos perfecciones hasta ser SIN FALTAS.

1. Nos prepara para encontraremos con el Señor.

    1. Pablo dio la verdad por el testimonio de Jesucristo.

        > "ASI que, hermanos, cuando fuí á vosotros, no fuí con altivez de palabra, ó de sabiduría, á anunciaros el testimonio de Cristo." -- 1 Corintios 2:1

    1. No debemos avergonsarnos del TESTIMONIO DE JESUCRISTO.

        > "Por tanto no te avergüences del testimonio de nuestro Señor, ni de mí, preso suyo; antes sé participante de los trabajos del evangelio según la virtud de Dios" -- 2 timotéo 1:8

    1. El ANGEL GABRIEL habla de este testimonio.

        > "El cual ha dado testimonio de la palabra de Dios, y del testimonio de Jesucristo, y de todas las cosas que ha visto." -- Apocalipsis 1:2

    1. Los martires murieron por el testimonio de jesucristo.

        > "Y cuando él abrió el quinto sello, vi debajo del altar las almas de los que habían sido muertos por la palabra de Dios y por el testimonio que ellos tenían." -- Apocalipsis 6:9

    1. Este testimonio es EL ESPIRITU DE PROFECIA.

        > "Y yo me eché á sus pies para adorarle. Y él me dijo: Mira que no lo hagas: yo soy siervo contigo, y con tus hermanos que tienen el testimonio de Jesús: adora á Dios; porque el testimonio de Jesús es el espíritu de la profecía." -- Apocalipsis 19:10

1. Hoy como antes necesitamos el espiritu de profecia, o sea, el testimonio de Jesucristo.

#### Cristo mismo repartio estos dones.

> "Empero hay repartimiento de dones;(Efecios dice que fue Jesucristo el que dio dones al hombre) mas el mismo Espíritu es." -- 1 Corintios 12:4

1. Este es el mismo espiritu que acompañó a los profetas y que sigue acompañando a la iglesia.

1. Negar el Espiritu de Profecia, es negar la Biblia.

    > Efesios 4:11; 1 Corintios 12:1, 28-31

    > Eran estas citas, este es DON DE LA IGLESIA CRISTIANA.

#### El don dell ESPIRITU DE PROFECIA es para dirigir la obra activa de la Iglesia.

> "Empero hay repartimiento de dones; mas el mismo Espíritu es. Y hay repartimiento de ministerios; mas el mismo Señor es. Y hay repartimiento de operaciones; mas el mismo Dios es el que obra todas las cosas en todos. Empero á cada uno le es dada manifestación del Espíritu para provecho. Porque á la verdad, á éste es dada por el Espíritu palabra de sabiduría; á otro, palabra de ciencia según el mismo Espíritu; A otro, fe por el mismo Espíritu, y á otro, dones de sanidades por el mismo Espíritu; A otro, operaciones de milagros, y á otro, profecía; y á otro, discreción de espíritus; y á otro, géneros de lenguas; y á otro, interpretación de lenguas. Mas todas estas cosas obra uno y el mismo Espíritu, repartiendo particularmente á cada uno como quiere. Porque de la manera que el cuerpo es uno, y tiene muchos miembros, empero todos los miembros del cuerpo, siendo muchos, son un cuerpo, así también Cristo." -- 1 Corintios 12:4-12

1. Todo esto para unir la Iglesia.

1. Entre los dones está El DE PROFECIA.

    > "SEGUID la caridad; y procurad los dones espirituales, mas sobre todo que profeticéis. Porque el que habla en lenguas, no habla á los hombres, sino á Dios; porque nadie le entiende, aunque en espíritu hable misterios. Mas el que profetiza, habla á los hombres para edificación, y exhortación, y consolación. El que habla lengua extraña, á sí mismo se edifica; mas el que porfetiza, edifica á la iglesia. Así que, quisiera que todos vosotros hablaseis lenguas, empero más que profetizaseis: porque mayor es el que profetiza que el que habla lenguas, si también no interpretare, para que la iglesia tome edificación." -- 1 Corintios 14:1-5

    1. La profecia EDIFICA EXHORTA Y CONSUELA.

#### Quienes serian elegidos como profetas. En la iglesia.

1. Los Hijos e Hijas. Hombres y mujeres.

    > Joel 2:28

    1. Se cumplió en PENDECOSTES. Hechos 2:1-4, 17

    1. Las hijas de Felipe. Hechos 21:6,9

    1. En la iglesia Cristiana. 1 Corintios 1:4-8

    1. Está en la iglesia REMANENTE. Apocalipsis 14:12

### Un paralelo profético

#### Experiencia de la SALIDA DE ISRAEL DE LA CAUTIVIDAD.

> "Entonces dijo á Abram: Ten por cierto que tu simiente será peregrina en tierra no suya, y servirá á los de allí, y serán por ellos afligidos cuatrocientos años. Mas también á la gente á quien servirán, juzgaré yo; y después de esto saldrán con grande riqueza. Y tú vendrás á tus padres en paz, y serás sepultado en buena vejez. Y en la cuarta generación volverán acá: porque aun no está cumplida la maldad del Amorrheo hasta aquí." -- Genesis 15:13-16

1. Librando en el momento mismo en que se cumplió la PROFECIA.

    > "Y pasados cuatrocientos treinta años, en el mismo día salieron todos los ejércitos de Jehová de la tierra de Egipto." -- Exodo 12:41

    1. Por profeta lo sacó del cautiverio.

        > "Y por profeta hizo subir Jehová á Israel de Egipto, y por profeta fué guardado." -- Oseas 12:13

#### Israel TIPO DEL PUEBLO ADVENTISTA.

1. En 1844 por profeta salió y se formó el PUEBLO ADVENTISTA.

    1. La hermana white ha guiado, guia y guiará a este pueblo.

1. Relación de la Profeta con la Biblia.

    1. No hizo ella una nueva Biblia.

    1. Armonía absoluta con la Biblia.

    > Isaias 8

1. La profetiza ha guiado en todo a la iglesia.

1. Su vida es consecuente con su predicción y enseñanza.

1. Condiciónes Físicas cuando entraba en visión.

    1. Visión con ojos abiertos. Números 24:15-16

    1. Perdia la fuerza. Daniel 6:18

    1. Luego se levantaba con fuerzas.

#### El espiritu de profecia se retira solo por apostasía

> Esequiel 20:1-12

## EPILOGO
