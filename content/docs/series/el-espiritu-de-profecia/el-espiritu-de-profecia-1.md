# 1/5 El Espíritu de Profecía

## EXORDIO

### El pecado cambio las relaciones del hombre para con Dios

1. Antes de la caida hablaban entre si personalmente.

    1. Pero no ha sido asi durante los casi 6000 años de la historia humana del hombre despues del pecado.

    2. El pecado causó este aislándose

        > "HE aquí que no se ha acortado la mano de Jehová para salvar, ni hase agravado su oído para oir: Mas vuestras iniquidades han hecho división entre vosotros y vuestro Dios, y vuestros pecados han hecho ocultar su rostro de vosotros, para no oir." -- Isaias 59:1-2

    1. Dios podía haber cortado toda comunicación conel hombre.

        1. Pero como Dios ES AMOR no pudo hacerlo.

        1. Encontró una manera para relacionarse con el hombre pecador sin destruirlo con su presencia.

## CUERPO

### Diversas maneras como ha revelado Dios su voluntad al hombre

1. Ya hemos dicho: NO MAS EN PERSONA.

    > "Quien sólo tiene inmortalidad, que habita en luz inaccesible; á quien ninguno de los hombres ha visto ni puede ver: al cual sea la honra y el imperio sempiterno. Amén." -- 1 Timoteo 6:16

    1. Si hablara con el hombre en persona, lo fulminaria, y LA MUERTE NO SE HABRIA POSTERGADO.

1. Por su voz Dios reveló su voluntad CUATRO VECES.

    1. Cuanto llamó a moises a su ministerio.

        > "Y viendo Jehová que iba á ver, llamólo Dios de en medio de la zarza, y dijo: ­Moisés, Moisés! Y él respondió: Heme aquí." -- Exodo 3:4

    1. En el bautismo de Jesus.

        > "Y aconteció que, como todo el pueblo se bautizaba, también Jesús fué bautizado; y orando, el cielo se abrió, Y descendió el Espíritu Santo sobre él en forma corporal, como paloma, y fué hecha una voz del cielo que decía: Tú eres mi Hijo amado, en ti me he complacido." -- Lucas  3:21-22

    1. En el monte de la transfiguracion.

        > "Y DESPUÉS de seis días, Jesús toma á Pedro, y á Jacobo, y á Juan su hermano, y los lleva aparte á un monte alto: Y se transfiguró delante de ellos; y resplandeció su rostro como el sol, y sus vestidos fueron blancos como la luz. Y he aquí les aparecieron Moisés y Elías, hablando con él. Y respondiendo Pedro, dijo á Jesús: Señor, bien es que nos quedemos aquí: si quieres, hagamos aquí tres pabellones: para ti uno, y para Moisés otro, y otro para Elías. Y estando aún él hablando, he aquí una nube de luz que los cubrió; y he aquí una voz de la nube, que dijo: Este es mi Hijo amado, en el cual tomo contentamiento: á él oíd." -- Mateo 12:1-5

    1. Una vez predicado jesús.

        > "Padre, glorifica tu nombre. Entonces vino una voz del cielo: Y lo he glorificado, y lo glorificaré otra vez. Y la gente que estaba presente, y había oído, decía que había sido trueno. Otros decían: Angel le ha hablado." -- Juan 12:28-29

1. Dios ha revelado su voluntad POR INTERMEDIO DE ANGELES.

    1. A Abraham le habló por medio de angeles.

    1. Lot habló con angeles mensajeros de Dios Padre.

    1. Maria recibió el mensaje de que sería la madre del prometido, por intermedio de un angel.

        1. La Biblia da hasta el Nombre del Angeles. Esto se llama GABRIEL.

1. Dios ha revelado su voluntad por  URIM Y TURIM.

    > "Y juntarán el racional con sus anillos á los anillos del ephod con un cordón de jacinto, para que esté sobre el cinto del ephod, y no se aparte el racional del ephod. Y llevará Aarón los nombres de los hijos de Israel en el racional del juicio sobre su corazón, cuando entrare en el santuario, para memoria delante de Jehová continuamente. Y pondrás en el racional del juicio Urim y Thummim, para que estén sobre el corazón de Aarón cuando entrare delante de Jehová: y llevará siempre Aarón el juicio de los hijos de Israel sobre su corazón delante de Jehová." -- Exodo 28:28-30

    1. Estas eran dos piedras con las cuales de consultaba la voluntad de Dios.

1. EL ESPIRITU DE PROFECIA el medio más usado por Dios para dar a conocer su voluntad al hombre.

    > "Porque no hará nada el Señor Jehová, sin que revele su secreto á sus siervos los profetas." -- Amos 3:7

    1. Si queremos estar bien informados, tenemos que consultar a los profetas Bíblicos, que nos ayudarán en el viaje de Edén a Edén.

### Estudio sobre el espiritu de profecia

1. Muchos Niegan la valicides del espiritu de profecia en la IGLESIA CRISTIANA.

    1. Piensan que la voluntad de Dios se revela por la palabrá del PAPA.

        1. De allí el dogma de LA INFALIBILIDAD PAPAL.

    1. Otros creen en la LA PALABRA DE LOS PASTORES.

1. Pero la profecia es una realidad del nuevo testamento.

    > "Y ACERCA de los dones espirituales, no quiero, hermanos, que ignoréis... Y á unos puso Dios en la iglesia, primeramente apóstoles, luego profetas, lo tercero doctores; luego facultades; luego dones de sanidades, ayudas, gobernaciones, géneros de lenguas." -- 1 Corintios 12:1,26

    1. El Señor dejó en su IGLESIA.

    1. El Término "Espiritu de profecia es biblico."

        > "Y yo me eché á sus pies para adorarle. Y él me dijo: Mira que no lo hagas: yo soy siervo contigo, y con tus hermanos que tienen el testimonio de Jesús: adora á Dios; porque el testimonio de Jesús es el espíritu de la profecía." -- Apocalipsis 19:10

    1. Pablo repite LA REPARTICION DE DONES POR CRISTO.

        > " Por lo cual dice: Subiendo á lo alto, llevó cautiva la cautividad, Y dió dones á los hombres. (Y que subió, ¿qué es, sino que también había descendido primero á las partes más bajas de la tierra? El que descendió, él mismo es el que también subió sobre todos los cielos para cumplir todas las cosas.) Y él mismo dió unos, ciertamente apóstoles; y otros, profetas; y otros, evangelistas; y otros, pastores y doctores; Para perfección de los santos, para la obra del ministerio, para edificación del cuerpo de Cristo; Hasta que todos lleguemos á la unidad de la fe y del conocimiento del Hijo de Dios, á un varón perfecto, á la medida de la edad de la plenitud de Cristo" -- Efesios 4:8-13

        1. Cristo que estos donde estaban en la iglesia, pero de una manera especial Cristo los confirmó en ella cuando SE FUE AL CIELO.

        1. Algunos creen que la profecia solo llegó hasta Juan, pero la cosa no es asi.

        1. La profecia quedó PARA PERFECCION DE LOS SANTOS.

        1. Estos dones estarían en la iglesia hasta que se terminara la obrainiciada por EL.

            - Asi como el arquitecto entrega planes y herramientas a los constructores y su retira, esperando que los constructores terminan la obra, Cristo entrgó los dones en manos de los hombres para que llevaran su Evangelio hasta el fin.

## EPILOGO

### Hacer uso de todos estos dones es la necesidad del momento

1. Hoy que hay tantos peligros y engañoa en el mundo religioso.

    1. Recordamos que no hay nada que hga el señor que no lo haya revelado a sus siervos los profetas.

1. Hoy cuando hay tanto sacerdocio y división en las iglesias.

    1. El espiritu de profecia y los otros dones ayudarán a unir EL PUEBLO MAS COSMOPOLITA DEL MUNDO.
